package edu.towson.cis.cosc442.project4.coffeemaker;

import junit.framework.TestCase;

/**
 *
 */
public class CoffeeMakerTest extends TestCase {
	private CoffeeMaker cm;
	private Inventory i, i2;
	private Recipe r1, r2, r3, r4, r5;

	public void setUp() {
		cm = new CoffeeMaker();
		i = cm.checkInventory();
		i2 = cm.checkInventory();

		r1 = new Recipe();
		r1.setName("Coffee");
		r1.setPrice(50);
		r1.setAmtCoffee(6);
		r1.setAmtMilk(1);
		r1.setAmtSugar(1);
		r1.setAmtChocolate(0);
	
	}

	//Add Recipe
	public void testAddRecipe1() {
		assertTrue(cm.addRecipe(r1));
	}
	
	public void testAddRecipeNegative() {
		r2 = new Recipe();
		r2.setName("Coffee");
		r2.setPrice(50);
		r2.setAmtCoffee(-1);
		r2.setAmtMilk(-1);
		r2.setAmtSugar(-1);
		r2.setAmtChocolate(-1);
		assertTrue(cm.addRecipe(r2));
	}
	

	//Delete Recipe
	public void testDeleteRecipe1() {
		cm.addRecipe(r1);
		assertTrue(cm.deleteRecipe(r1));
	}

	public void testEditRecipe1() {
		cm.addRecipe(r1);
		Recipe newRecipe = new Recipe();
		newRecipe = r1;
		newRecipe.setAmtSugar(2);
		assertTrue(cm.editRecipe(r1, newRecipe));
	}
	

	public void testDeleteRecipe2() {
		assertFalse(cm.deleteRecipe(null));
	}
	
	
	//Check Recipe
	public void testcheckEmptyRecipe() {
		r2 = new Recipe();
		r2.setName("Coffee2");
		r2.setPrice(50);
		r2.setAmtCoffee(-1);
		r2.setAmtMilk(3);
		r2.setAmtSugar(3);
		r2.setAmtChocolate(3);
		assertTrue(cm.addRecipe(r1));
		
	}
	
	public void testEmptyRecipeCoffee() {
		i.setCoffee(-1);
		assertEquals(i.getCoffee(), 0);
		
	}
	public void testZeroRecipeCoffee() {
		i.setCoffee(0);
		cm.addInventory(0, 2, 3, 3);
		assertEquals(i.getCoffee(), 0);
		
	}
	
	
	public void testEmptyRecipeMilk() {
		i.setMilk(-1);
		assertEquals(i.getMilk(), 0);
		
	}
	public void testZeroRecipeMilk() {
		i.setMilk(0);
		assertEquals(i.getMilk(), 0);
		
	}
	public void testOneRecipeMilk() {
		i.setMilk(1);
		assertEquals(i.getMilk(), 1);
		
	}
	
	
	public void testEmptyRecipeSugar() {
		i.setSugar(-1);
		assertEquals(i.getSugar(), 0);
		
	}
	public void testZeroRecipeSugar() {
		i.setSugar(0);
		assertEquals(i.getSugar(), 0);
		
	}
	public void testOneRecipeSugar() {
		i.setSugar(1);
		assertEquals(i.getSugar(), 1);
		
	}
	
	
	public void testEmptyRecipeChocolate() {
		i.setChocolate(-1);
		assertEquals(i.getChocolate(), 0);
		
	}
	public void testZeroRecipeChocolate() {
		i.setChocolate(0);
		assertEquals(i.getChocolate(), 0);
		
	}
	public void testOneRecipeChocolate() {
		i.setChocolate(1);
		assertEquals(i.getChocolate(), 1);
		
	}
	
	public void testEmptyRecipePrice() {
		r2 = new Recipe();
		r2.setName("Coffee");
		r2.setPrice(-1);
		r2.setAmtCoffee(3);
		r2.setAmtMilk(2);
		r2.setAmtSugar(3);
		r2.setAmtChocolate(3);
		assertEquals(r2.getPrice(), 0);
		
	}
	
	//Check Inventory
	public void testcheckInventoryChocolate() {
		assertEquals(i2.getChocolate(), 15);
		
	}
	public void testcheckInventoryCoffee() {
		assertEquals(i2.getCoffee(), 15);

	}
	public void testcheckInventoryMilk() {
		assertEquals(i2.getMilk(), 15);
		
	}
	public void testcheckInventorySugar() {
		assertEquals(i2.getSugar(), 15);
		
	}
	public void testaddInvetory() {
		assertEquals(cm.addInventory(3, 2, 3, 3), true);
	}
	
	
	public void testGetRecipeForName(){
		cm.addRecipe(r1);
		
		r2 = cm.getRecipeForName(r1.getName());
		String nameTest = r2.getName();
		assertTrue(nameTest.equalsIgnoreCase("Coffee"));
	}
	
	
	public void testInsufficientMilk() {
		final Recipe r1 = new Recipe();
		r1.setName("Coffee");
		r1.setPrice(50);
		r1.setAmtCoffee(6);
		r1.setAmtMilk(15);
		r1.setAmtSugar(1);
		r1.setAmtChocolate(0);
		assertTrue("Should have had enough milk for recipe", i.enoughIngredients(r1));
	}
	
	public void testInsufficientSugar() {
		final Recipe r1 = new Recipe();
		r1.setName("Coffee");
		r1.setPrice(50);
		r1.setAmtCoffee(6);
		r1.setAmtMilk(1);
		r1.setAmtSugar(15);
		r1.setAmtChocolate(0);
		assertTrue("Should have had enough sugar for recipe", i.enoughIngredients(r1));
	}
	
	public void testInsufficientChocolate() {
		final Recipe r1 = new Recipe();
		r1.setName("Coffee");
		r1.setPrice(50);
		r1.setAmtCoffee(6);
		r1.setAmtMilk(1);
		r1.setAmtSugar(1);
		r1.setAmtChocolate(15);
		assertTrue("Should have had enough chocolate for recipe", i.enoughIngredients(r1));
	}
	public void testInsufficientCoffee() {
		final Recipe r1 = new Recipe();
		r1.setName("Coffee");
		r1.setPrice(50);
		r1.setAmtCoffee(15);
		r1.setAmtMilk(1);
		r1.setAmtSugar(1);
		r1.setAmtChocolate(0);
		assertTrue("Should have had enough for coffee", i.enoughIngredients(r1));
	}
	
	
	
	//Add Inventory
	public void testAddInventory4(){
		assertTrue(cm.addInventory(1, 1, 1, 1));
		assertTrue(i.getChocolate() == 16);
		assertTrue(i.getCoffee() == 16);
		assertTrue(i.getMilk() == 16);
		assertTrue(i.getSugar() == 16);
	}
	
	public void testaddInventoryZero() {
		assertEquals(cm.addInventory(0, 0, 0, 0), true);
	}
	public void testaddInventoryNegative() {
		assertEquals(cm.addInventory(3, 2, -1, 3), false);
	}
	
	public void testmakeCoffeeHighNumbers() {
		cm.addRecipe(r1);
		i.setCoffee(20);
		i.setMilk(20);
		i.setSugar(20);
		i.setChocolate(20);
		cm.addInventory(30, 26, 33, 300);
		final Recipe [] recipes = cm.getRecipes();
		final Recipe recipe = recipes[3];
		assertEquals(cm.makeCoffee(recipe, 50), 0);
	}
	
	
	//Make Coffee
	public void testmakeCoffee() {
		cm.addRecipe(r1);
		final Recipe [] recipes = cm.getRecipes();
		final Recipe recipe = recipes[3];
		assertEquals(cm.makeCoffee(recipe, 75), 25);
	}
	
	public void testmakeCoffeLessPrice() {
		cm.addRecipe(r1);
		final Recipe [] recipes = cm.getRecipes();
		final Recipe recipe = recipes[3];
		assertEquals(cm.makeCoffee(recipe, 35), 35);
	}
	
	public void testmakeCoffeeLessProduct() {
		r2 = new Recipe();
		r2.setName("Coffee");
		r2.setPrice(50);
		r2.setAmtCoffee(20);
		r2.setAmtMilk(1);
		r2.setAmtSugar(1);
		r2.setAmtChocolate(0);
		cm.addRecipe(r2);
		final Recipe [] recipes = cm.getRecipes();
		final Recipe recipe = recipes[3];
		assertEquals(cm.makeCoffee(recipe, 60), 60);
	}
	
	public void testmakeMilkLessProduct() {
		r2 = new Recipe();
		r2.setName("Coffee");
		r2.setPrice(50);
		r2.setAmtCoffee(1);
		r2.setAmtMilk(50);
		r2.setAmtSugar(1);
		r2.setAmtChocolate(1);
		cm.addRecipe(r2);
		final Recipe [] recipes = cm.getRecipes();
		final Recipe recipe = recipes[3];
		assertEquals(cm.makeCoffee(recipe, 60), 60);
	}
	
	public void testmakeSugarLessProduct() {
		r2 = new Recipe();
		r2.setName("Coffee");
		r2.setPrice(50);
		r2.setAmtCoffee(1);
		r2.setAmtMilk(1);
		r2.setAmtSugar(50);
		r2.setAmtChocolate(1);
		cm.addRecipe(r2);
		final Recipe [] recipes = cm.getRecipes();
		final Recipe recipe = recipes[3];
		assertEquals(cm.makeCoffee(recipe, 60), 60);
	}
	
	public void testmakeChocolateLessProduct() {
		r2 = new Recipe();
		r2.setName("Coffee");
		r2.setPrice(50);
		r2.setAmtCoffee(1);
		r2.setAmtMilk(1);
		r2.setAmtSugar(1);
		r2.setAmtChocolate(50);
		cm.addRecipe(r2);
		final Recipe [] recipes = cm.getRecipes();
		final Recipe recipe = recipes[3];
		assertEquals(cm.makeCoffee(recipe, 60), 60);
	}
	
	//Other tests
	
	public void testReturnOutput() {
		cm.addRecipe(r1);
		assertEquals(i.toString(), String.format("Coffee: %s%n Milk: %s%n Sugar: %s%n Chocalate: %s%n", 
			 	i.getCoffee(), i.getMilk(), i.getSugar(), i.getChocolate()));
		
	}
	
	public void testEmptyArrayRecipe() {
		
		r2 = new Recipe();
		r2.setName("Coffee2");
		r2.setPrice(50);
		r2.setAmtCoffee(6);
		r2.setAmtMilk(1);
		r2.setAmtSugar(1);
		r2.setAmtChocolate(0);
		
		r3 = new Recipe();
		r3.setName("Coffee3");
		r3.setPrice(50);
		r3.setAmtCoffee(6);
		r3.setAmtMilk(1);
		r3.setAmtSugar(1);
		r3.setAmtChocolate(0);
		
		r4 = new Recipe();
		r4.setName("Coffee4");
		r4.setPrice(50);
		r4.setAmtCoffee(6);
		r4.setAmtMilk(1);
		r4.setAmtSugar(1);
		r4.setAmtChocolate(0);
		
		r5 = new Recipe();
		r5.setName("Coffee5");
		r5.setPrice(50);
		r5.setAmtCoffee(6);
		r5.setAmtMilk(1);
		r5.setAmtSugar(1);
		r5.setAmtChocolate(0);
		
		cm.addRecipe(r1);
		cm.addRecipe(r2);
		cm.addRecipe(r3);
		cm.addRecipe(r4);
		
		assertEquals(cm.addRecipe(r5), false);
		
	}
	
	public void testRecipeExist() {
		cm.addRecipe(r1);
		assertEquals(cm.addRecipe(r1), false);
	}
	
	public void testEqualsWithOtherNull() {
		r1.setName("Coffee");
		final Recipe r2 = new Recipe();
		assertFalse("Returned that recipes were equal", r1.equals(r2));
	}
	
	public void testEqualsWithThisNull() {
		final Recipe r2 = new Recipe();
		r2.setName(null);
		assertFalse("Returned that recipes were equal", r1.equals(r2));
	}

	public void testEqualsWithDiffName() {
		final Recipe r2 = new Recipe();
		r1.setName("Coffee");
		r2.setName("Latte");
		assertFalse("Returned that recipes were equal", r1.equals(r2));
	}
	
	public void testToString() {
		r1.setName("Coffee");
		assertEquals("Incorrect name", "Coffee", r1.toString());
	}
	
	
}